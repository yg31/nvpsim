import os, optparse, sys

import m5
from m5.defines import buildEnv
from m5.objects import *
from m5.util import addToPath

addToPath('../common')
addToPath('../ruby')
addToPath('../topologies')

import Options
import Ruby
import Simulation
from Caches import *
from cpu2000 import *
import CacheConfig
import mibenchAll


config_path = os.path.dirname(os.path.abspath(__file__))
config_root = os.path.dirname(config_path)
m5_root = os.path.dirname(config_root)

parser = optparse.OptionParser()
Options.addCommonOptions(parser)
Options.addSEOptions(parser)

parser.add_option("--benchmark",type="string",default="",help="The benchmark to be loaded.")

parser.add_option("--chkpt",default="",help="The checkpoint to load.")

execfile(os.path.join(config_root,"common","Options.py"))


if '--ruby' in sys.argv:
    Ruby.define_options(parser)

(options,args) = parser.parse_args()


if args:
    sys.exit(1)

if options.benchmark == 'basicmath_large':
    process = mibenchAll.basicmath_large
elif options.benchmark == 'basicmath_small':
    process = mibenchAll.basicmath_small
elif options.benchmark == 'bitcount':
    process = mibenchAll.bitcount
elif options.benchmark == 'qsort_large':
    process = mibenchAll.qsort_large
elif options.benchmark == 'qsort_small':
    process = mibenchAll.qsort_small
elif options.benchmark == 'susan_s':
    process = mibenchAll.susan_s
elif options.benchmark == 'susan_e':
    process = mibenchAll.susan_e
elif options.benchmark == 'susan_c':
    process = mibenchAll.susan_c
elif options.benchmark == 'cjpeg':
    process = mibenchAll.cjpeg
elif options.benchmark == 'dijkstra':
    process = mibenchAll.dijkstra
elif options.benchmark == 'patricia':
    process = mibenchAll.patricia
elif options.benchmark == 'djpeg':
    process = mibenchAll.djpeg
elif options.benchmark == 'lame':
    process = mibenchAll.lame
elif options.benchmark == 'typeset':
    process = mibenchAll.typeset
elif options.benchmark == 'sha':
    process = mibenchAll.sha
elif options.benchmark == 'blowfish_e':
    process = mibenchAll.blowfish_e
elif options.benchmark == 'rijndael_e':
    process = mibenchAll.rijndael_e
elif options.benchmark == 'crc':
    process = mibenchAll.crc
elif options.benchmark == 'FFT':
    process = mibenchAll.FFT
elif options.benchmark == 'FFT_i':
    process = mibenchAll.FFT_i
elif options.benchmark == 'gsm_toast':
    process = mibenchAll.gsm_toast
elif options.benchmark == 'gsm_untoast':
    process = mibenchAll.gsm_untoast
elif options.benchmark == 'stringsearch':
    process = mibenchAll.stringsearch

if options.chkpt != "":
    process.chkpt = options.chkpt

(CPUClass, test_mem_mode, FutureClass) = Simulation.setCPUClass(options)

numThreads = 1
CPUClass.numThreads = numThreads;

np = options.num_cpus;

system = System(cpu = [CPUClass(cpu_id=i) for i in xrange(np)],
		mem_mode = test_mem_mode,
		mem_ranges = [AddrRange(options.mem_size)],
		cache_line_size = options.cacheline_size)

system.voltage_domain = VoltageDomain(voltage = options.sys_voltage)

system.clk_domain = SrcClockDomain(clock = options.sys_clock,voltage_domain = system.voltage_domain)

system.cpu_voltage_domain = VoltageDomain()

system.cpu_clk_domain = SrcClockDomain(clock =
	options.cpu_clock,voltage_domain = system.cpu_voltage_domain)


for cpu in system.cpu:
    cpu.clk_domain = system.cpu_clk_domain
    #if CPUClass == AtomicSimpleCPU:
#	cpu.simulate_data_stalls = True 
#	cpu.simulate_inst_stalls = True 
    cpu.workload = process

if options.fastmem:
    if CPUClass != AtomicSimpleCPU:
	fatal("Fastmem can only be used with atomic CPU!")
    if (options.caches or options.l2cache):
	fatal("You cannot use fastmem in combination with caches!")

if options.simpoint_profile:
    if not options.fastmem:
	fatal("SimPoint generation should be done with atomic cpu and fastmem")
    if np > 1:
	fatal("SimPoint generation not supported with more than one CPUs")

#for i in xrange(np):
#    system.cpu[i].workload = process

MemClass = Simulation.setMemClass(options)
system.membus = CoherentBus()
system.system_port = system.membus.slave
CacheConfig.config_cache(options,system)
MemConfig.config_mem(options,system)

#system.trigger = Trigger(cpu = system.cpu[0], cache = system.cpu[0].icache,backup_start = options.backup_start,resume_start=options.resume_start,pro_file=options.profile)

#system.trigger = Trigger(cpu = system.cpu[0], backup_start = options.backup_start,resume_start=options.resume_start,pro_file=options.profile)

#system.voltage_detection = VoltageDetection(cpu = system.cpu[0],vd_on=options.vd_on,icache=system.cpu[0].icache,dcache=system.cpu[0].dcache,sysParamPath=options.sys_param_file,powerTracePath=options.power_trace_file,strategy=options.bkp_stg)

system.voltage_detection = VoltageDetection(cpu=system.cpu[0],icache=system.cpu[0].icache,dcache=system.cpu[0].dcache,sysParamPath=options.sys_param_file,powerTracePath=options.power_trace_file,vd_on=options.vd_on,strategy=options.strategy,threshold=options.threshold)

root = Root(full_system = False, system = system)
Simulation.run(options,root,system,FutureClass)
