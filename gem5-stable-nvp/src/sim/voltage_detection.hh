#ifndef __SIM_VOLTAGE_DETECTION_HH__
#define __SIM_VOLTAGE_DETECTION_HH__
#include <pthread.h>
#include <string.h>
#include "params/VoltageDetection.hh"
#include "sim/sim_object.hh"
#include "cpu/simple/base.hh"
#include "cpu/simple_thread.hh"
#include "mem/cache/base.hh"
#include "sim/eventq.hh"
#include "sim/eventq_impl.hh"

void* proxy_run(void* ptr);

class VoltageDetection : public SimObject {
    public:
	typedef VoltageDetectionParams Params;
	VoltageDetection(const Params *p);
	~VoltageDetection();
	BaseSimpleCPU * _cpu;
	BaseCache * _icache;
	BaseCache * _dcache;
	std::string _sysParamPath;  	
	std::string _powerTracePath;			

	struct SysParam {
	    SysParam();
	    double C_bulk;
	    double P_comp;
	    double P_in;
	    double P_max;
	    double P_leak;

	    double t_nvsram_store;// nvsram cell store time
    	    double t_nvsram_restore; //nvsram cell restore time
	    double e_nvsram_store;// nvsram cell store energy
	    double e_nvsram_restore;// nvsram cell restore energy

	    double t_nvff_store;
	    double t_nvff_restore;
	    double e_nvff_store;
	    double e_nvff_restore;
    
	    double e_nvm_cache_hit; 
	    double e_nvm_cache_miss;
	    double e_nvm_cache_write;
	    double P_nvm_cache_leak;
	    double t_nvm_cache_hit_lat;
	    double t_nvm_cache_miss_lat;
	    double t_nvm_cache_write_lat;


	    double e_sram_cache_hit; 
	    double e_sram_cache_miss;
	    double e_sram_cache_write;
	    double P_sram_cache_leak;
	    double t_sram_cache_hit_lat;
	    double t_sram_cache_miss_lat;
	    double t_sram_cache_write_lat;
	    

	    double e_memory_read;// memory read dynamic energy	 
	    double e_memory_write;// memory write dynamic energy
	    double P_memory_leak;
	    double t_memory_read_lat;
	    double t_memory_write_lat;	

	    double t_plh; //latency of restore signal
	    double t_phl; //latency of store signal

	    double v_det_backup;
	    double v_det_restore; 
	    double v_min;		
	    double v_max;
	    double init_coef;

	    double Freq;
	    double duty_cycle; 			
	   // char power_trace_type[];
	    int power_trace_type;
	    double power_trace_scale;
	    double pt_t_end;
	    double pt_delta_t;
	    
	};
	
	struct Statistics { // focus on the 
	    void print_stats(FILE * fp);			
	    double backup_energy;
	    double resume_energy;
	    double cache_readhit_energy;
	    double cache_readmiss_energy;
	    double cache_writehit_energy;
	    double cache_writemiss_energy;
	    double cache_write_energy;
	    double mem_read_energy;
	    double mem_write_energy;
	    double other_energy;

	    double cache_leak_energy;
	    double cap_leak_energy;
	    double mem_leak_energy;
		
	    double input_energy;
	    double cap_diff_energy;

	    double backup_time;
	    double resume_time;
	    double cache_readhit_time;
	    double cache_readmiss_time;
	    double cache_writehit_time;
	    double cache_writemiss_time;
	    double cache_write_time;
	    double mem_read_time;
	    double mem_write_time;
	    double other_time;
	    double off_time;
	};

	int _vd_on;

	int _strategy;

	int _threshold; // the lru backup threshold

	SysParam sysp;

	Statistics stats;

	bool real_power_trace;	

	double p_backup;

	double p_resume;

	double p_nvff_backup;

	double p_nvff_resume;

	double p_nvsram_backup;

	double p_nvsram_resume; 	 

	double p_nvff_store;

	double p_nvff_restore;

	double p_nvsram_store;

	double p_nvsram_restore; 

	double p_nvm_cache_hit;

	double p_nvm_cache_miss;

	double p_nvm_cache_write;

	double p_sram_cache_hit;

	double p_sram_cache_miss;

	double p_sram_cache_write;

	double p_memory_read;

	double p_memory_write;  

	double p_real_in; 

	double v_sys;
	
	double v_init;

	double* p_t;

	double sim_delta_t;			 	 	 	

	uint64_t sim_delta_tick;

	uint64_t iCachebit; 

	uint64_t dCachebit; 

	uint64_t tick_pt_end;

	uint64_t cur_tick;
    
	uint64_t tick_on;
	
	uint64_t tick_off;  

	uint64_t tick_resume;

	uint64_t tick_vback; // tick when v_sys meet v_det_backup

	uint64_t tick_vrestore;

	uint64_t rollback_tick;

	uint64_t tick_nvsram_store;// nvsram cell store time just one cell!!

	uint64_t tick_nvsram_restore; //nvsram cell restore time just one cell!

	uint64_t tick_nvff_store;

	uint64_t tick_nvff_restore;

	uint64_t tick_backup_start;

	uint64_t tick_resume_start;
	
	uint64_t tick_rollback_start_local;

	uint64_t plh_tick; //latency of restore signal

	uint64_t phl_tick; //latency of store signal
	    
	uint64_t delay_tick_local;	

	uint64_t tick_next_local;

	uint64_t backup_tick;

	uint64_t resume_tick;	

	uint64_t backup_nvff_tick;

	uint64_t resume_nvff_tick;

	uint64_t backup_nvsram_tick;

	uint64_t resume_nvsram_tick;	 	

	long size;

	typedef enum {OFF,ON2OFF,ON,RESUME,BACKUP,OFF2ON,ROLLBACK2OFF,ROLLBACK}STATE;
	//typedef enum {ON,RESUME,OFF,BACKUP,ON2OFF,OFF2ON,ROLLBACK2OFF,ROLLBACK} STATE;

	STATE state;

	FILE * file_vsys;
	FILE * file_state;
	FILE * file_pt;
	FILE * file_sys_state;
	FILE * file_et_stats;
	void setPowerTrace(); 
	void setSysParam();	
	void calc_param();
	void dyn_calc_param(uint64_t n);

	void do_stat(struct Statistics& stats, int sys_state,double delta_t, double p_in);

	uint64_t t2tick(double t);
	double deltaV(double v, double p_out, double p_in, double C,double v_max, double delta_t, double& p_real_in);
	double calcOutPower(int state);
	void  run();
	void create_thread();
	void sched_resume(uint64_t,VoltageDetection *vd);
	pthread_t m_thread;
	friend void* proxy_run(void*);
    private:
	void generatePowerTrace();
	int getIndex(uint64_t cur_tick);
	void printStats();
	void print2file(FILE* fp1, FILE* fp2,FILE* fp3, FILE* fp4,double state, int vsys,double p_t);
	double getCapEnergyDiff(double init, double cur);
	double getPowerFromGlobalState();
	void nvffBackupPowerAndTime(double& p,double &t); 		      
	void nvffRestorePowerAndTime(double& p,double &t); 		
	void nvSRAMBackupPowerAndTime(const char* strategy,double& p,double &t); 	       
	void nvSRAMRestorePowerAndTime(double& p,double &t); 	       
	void strategy_1();
	void strategy_2();
	void strategy_3();
};


#endif
