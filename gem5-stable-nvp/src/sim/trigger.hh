#ifndef __SIM_TRIGGER_HH__
#define __SIM_TRIGGER_HH__

#include "cpu/simple/base.hh"
#include "cpu/simple_thread.hh"
#include "mem/cache/base.hh"
#include "sim/eventq.hh"
#include "sim/eventq_impl.hh"
#include "sim/sim_object.hh"
#include "params/Trigger.hh"
#include <string>


class Trigger : public SimObject {
    public:
	typedef TriggerParams Params;	

	Trigger(const Params *p);

	void regStats();

	struct BackupEvent : public Event {
	    BaseSimpleCPU * baseCpu;
	  //  BaseCache    * baseCache;  			 
	    uint64_t resume_tick;
	    void process();
//	    BackupEvent(BaseSimpleCPU *cpu, BaseCache *cache);
	    BackupEvent(BaseSimpleCPU *cpu);
	    const char * description() const;
	};

	struct ResumeEvent : public Event {
	    BaseSimpleCPU * baseCpu;
	  //  BaseCache    * baseCache;
	    void process();
//	    ResumeEvent(BaseSimpleCPU *cpu, BaseCache *cache);
	    ResumeEvent(BaseSimpleCPU *cpu);
	    const char * description() const;
	};

	BackupEvent backupEvent;
	ResumeEvent resumeEvent;
	uint64_t backupStart;
	uint64_t resumeStart;
	std::string pro_file;	
};

#endif
