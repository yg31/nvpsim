#include "debug/VoltageDetection.hh"
#include "params/VoltageDetection.hh"
#include "sim/voltage_detection.hh"

void *proxy_run(void* ptr) {
    static_cast<VoltageDetection*>(ptr)->run();
    pthread_exit(NULL);
}

void VoltageDetection::create_thread() {
    pthread_create(&m_thread,NULL,proxy_run,this);
}

VoltageDetection::~VoltageDetection() {
    delete[] p_t;
    fclose(file_vsys);
    fclose(file_state);
    fclose(file_pt);
    fclose(file_sys_state);
    fclose(file_et_stats);
}

VoltageDetection::VoltageDetection(const Params *p) : SimObject(p),_cpu(p->cpu),_icache(p->icache),_dcache(p->dcache),_sysParamPath(p->sysParamPath),_powerTracePath(p->powerTracePath),_vd_on(p->vd_on),_strategy(p->strategy),_threshold(p->threshold) {
    DPRINTF(VoltageDetection,"%s\n",__func__);
    vd_run_flag = false;  
    printf("VoltageDetection init\n");
    setSysParam();
    setPowerTrace();
    cur_tick = 0;
    off_flag = false;
    resched_flag = false;
    file_vsys = fopen("v_sys.txt","w");
    file_state = fopen("state.txt","w");
    file_pt = fopen("pt.txt","w");
    file_sys_state = fopen("sys_state.txt","w");
    file_et_stats = fopen("energy_time_stats.txt","w");
    state = ON;
    DPRINTF(VoltageDetection,"%s create thread\n",__func__);
    printf("size: %ld\n",size);
    iCachebit = _icache->cacheInfo();
    dCachebit = _dcache->cacheInfo();
    v_sys = sysp.v_det_backup + (sysp.v_max - sysp.v_det_backup) * sysp.init_coef;
    v_init = v_sys;
    vd_on = _vd_on;
    create_thread();
}

void VoltageDetection::print2file(FILE * fp_vsys, FILE* fp_state, FILE* fp_pt, FILE * fp_sys,double vsys,int state,double p_t) {
    fprintf(fp_vsys,"%ld %lf\n",cur_tick, v_sys);
    fprintf(fp_state,"%ld %d\n",cur_tick,state);	
    fprintf(fp_pt,"%ld %lf\n",cur_tick, p_t);
    fprintf(fp_sys,"%ld %d\n",cur_tick,gstatus);
}

void VoltageDetection::dyn_calc_param(uint64_t N) {
    /*
	double n = sysp.I_constraint/sysp.i_nvsram_store * 1000;
	sysp.t_nvsram_store= (double)N/n * sysp.t_nvsram_store/1000000000.0;
	sysp.tick_nvsram_store = t2tick(sysp.t_nvsram_store);

	n = sysp.I_constraint/sysp.i_nvsram_restore * 1000;
	sysp.t_nvsram_restore = (double)N/n * sysp.t_nvsram_restore/1000000000.0;
	sysp.tick_nvsram_restore = t2tick(sysp.t_nvsram_restore);
	*/

}

uint64_t VoltageDetection::t2tick(double t) {
    return (uint64_t)(t * 1000000000000.0);
}

void VoltageDetection::calc_param() {
    /* 
	double n = sysp.I_constraint/sysp.i_nvsram_store;
	sysp.t_nvsram_store = (double)dCachebit/n * sysp.t_nvsram_store;
	sysp.tick_nvsram_store = t2tick(sysp.t_nvsram_store);
	sysp.t_nvsram_restore = (double)dCachebit/n * sysp.t_nvsram_restore;
	sysp.tick_nvsram_restore = t2tick(sysp.t_nvsram_restore);
    */
}

void VoltageDetection::setSysParam() {
    printf("set sys params\n");
    FILE * fp = fopen(_sysParamPath.c_str(),"r");
    double thousand = 1000;
    double million = thousand * 1000;
    double billion = million * 1000;
    double trillion = billion * 10000;
    if(fp != NULL) {
	fscanf(fp,"%lf",&sysp.C_bulk);
	fscanf(fp,"%lf",&sysp.P_comp);
	fscanf(fp,"%lf",&sysp.P_in);
	fscanf(fp,"%lf",&sysp.P_max);
	fscanf(fp,"%lf",&sysp.P_leak);
	fscanf(fp,"%lf",&sysp.pt_delta_t);
	fscanf(fp,"%lf",&sysp.t_nvsram_store);
	fscanf(fp,"%lf",&sysp.t_nvsram_restore);
	fscanf(fp,"%lf",&sysp.e_nvsram_store);
	fscanf(fp,"%lf",&sysp.e_nvsram_restore);
	fscanf(fp,"%lf",&sysp.t_nvff_store);
	fscanf(fp,"%lf",&sysp.t_nvff_restore);
	fscanf(fp,"%lf",&sysp.e_nvff_store);
	fscanf(fp,"%lf",&sysp.e_nvff_restore);
	fscanf(fp,"%lf",&sysp.e_nvm_cache_hit);
	fscanf(fp,"%lf",&sysp.e_nvm_cache_miss);
	fscanf(fp,"%lf",&sysp.e_nvm_cache_write);
	fscanf(fp,"%lf",&sysp.P_nvm_cache_leak);
	fscanf(fp,"%lf",&sysp.t_nvm_cache_hit_lat);
	fscanf(fp,"%lf",&sysp.t_nvm_cache_miss_lat);
	fscanf(fp,"%lf",&sysp.t_nvm_cache_write_lat);
	fscanf(fp,"%lf",&sysp.e_sram_cache_hit);
	fscanf(fp,"%lf",&sysp.e_sram_cache_miss);
	fscanf(fp,"%lf",&sysp.e_sram_cache_write);
	fscanf(fp,"%lf",&sysp.P_sram_cache_leak);
	fscanf(fp,"%lf",&sysp.t_sram_cache_hit_lat);
	fscanf(fp,"%lf",&sysp.t_sram_cache_miss_lat);
	fscanf(fp,"%lf",&sysp.t_sram_cache_write_lat);
	fscanf(fp,"%lf",&sysp.e_memory_read);
	fscanf(fp,"%lf",&sysp.e_memory_write);
	fscanf(fp,"%lf",&sysp.t_memory_read_lat);
	fscanf(fp,"%lf",&sysp.t_memory_write_lat);
	fscanf(fp,"%lf",&sysp.P_memory_leak);
	fscanf(fp,"%lf",&sysp.t_plh);
	fscanf(fp,"%lf",&sysp.t_phl);
	fscanf(fp,"%lf",&sysp.v_det_backup);
	fscanf(fp,"%lf",&sysp.v_det_restore);
	fscanf(fp,"%lf",&sysp.v_min);
	fscanf(fp,"%lf",&sysp.v_max);
	fscanf(fp,"%lf",&sysp.init_coef);
	fscanf(fp,"%lf",&sysp.Freq);
	fscanf(fp,"%lf",&sysp.duty_cycle);
	fscanf(fp,"%d",&sysp.power_trace_type);
	fscanf(fp,"%lf",&sysp.power_trace_scale);
	fscanf(fp,"%lf",&sysp.pt_t_end);
	fscanf(fp,"%ld",&sim_delta_tick);

	sysp.C_bulk /= billion;
	sysp.P_comp /= million;
	sysp.P_in /= million;
	sysp.P_max /= million;
	sysp.P_leak /= million;		
	sysp.t_nvsram_store /= million;
	sysp.t_nvsram_restore /= million;
	sysp.e_nvsram_store /= trillion;
	sysp.e_nvsram_restore /= trillion;
	sysp.t_nvff_store /= million;
	sysp.t_nvff_restore  /= million;
	sysp.e_nvff_store /= trillion;
	sysp.e_nvff_restore /= trillion;
	sysp.e_nvm_cache_hit /= billion;
	sysp.e_nvm_cache_miss /= billion;
	sysp.e_nvm_cache_write /= billion;
	sysp.P_nvm_cache_leak /= thousand;
	sysp.t_nvm_cache_hit_lat /= billion;
	sysp.t_nvm_cache_miss_lat /= billion; 
	sysp.e_sram_cache_hit /= billion;
	sysp.e_sram_cache_miss /= billion;
	sysp.e_sram_cache_write /= billion;
	sysp.P_sram_cache_leak /= thousand;
	sysp.t_sram_cache_hit_lat /= billion;
	sysp.t_sram_cache_miss_lat /= billion; 
	sysp.e_memory_read /= trillion;
	sysp.e_memory_write /= trillion;
	sysp.t_memory_read_lat /=  billion;
	sysp.t_memory_write_lat /= billion;
	sysp.P_memory_leak /= thousand;

	sysp.t_plh /= million;
	sysp.t_phl /= million;	
	sysp.Freq *= thousand;
	sysp.pt_t_end /= thousand;
	sysp.pt_delta_t /= million;

	tick_nvsram_store = t2tick(sysp.t_nvsram_store);	
	tick_nvsram_restore = t2tick(sysp.t_nvsram_restore);

	tick_nvff_store = t2tick(sysp.t_nvff_store);
	tick_nvff_restore = t2tick(sysp.t_nvff_restore);

	phl_tick = t2tick(sysp.t_phl);
	plh_tick = t2tick(sysp.t_plh);

	tick_pt_end = t2tick(sysp.pt_t_end);	 	

	sim_delta_t = (double)sim_delta_tick / trillion;
    
	p_nvm_cache_hit = sysp.e_nvm_cache_hit/sysp.t_nvm_cache_hit_lat; 
	p_nvm_cache_miss = sysp.e_nvm_cache_miss/sysp.t_nvm_cache_miss_lat;	
	p_nvm_cache_write = sysp.e_nvm_cache_write/sysp.t_nvm_cache_write_lat;

	p_sram_cache_hit = sysp.e_sram_cache_hit/sysp.t_sram_cache_hit_lat; 
	p_sram_cache_miss = sysp.e_sram_cache_miss/sysp.t_sram_cache_miss_lat;	
	p_sram_cache_write = sysp.e_sram_cache_write/sysp.t_sram_cache_write_lat;

	p_memory_read = sysp.e_memory_read/sysp.t_memory_read_lat;
	p_memory_write = sysp.e_memory_write/sysp.t_memory_write_lat;

	p_nvff_restore = sysp.e_nvff_restore/sysp.t_nvff_restore;
	p_nvff_store = sysp.e_nvff_store/sysp.t_nvff_store;

    } else {
	printf("fp == NULL\n");
	exit(1);
    }
}


void VoltageDetection::generatePowerTrace() { // generate
    printf("generate Power Trace\n");
    double T = 1/(sysp.Freq);   
    int N = (int)(sysp.pt_t_end/sysp.pt_delta_t) + 1;
    int n = int(T/sysp.pt_delta_t);
    printf("T = %lf , N = %d, n = %d P_in = %lf\n", T,N,n,sysp.P_in);
    p_t = new double[N];
    assert(sysp.duty_cycle < 1);
    int cnt = 0;
    int t1 = int(sysp.duty_cycle * (double)n);
    for(int i = 0; i < N; ++i) {
	if(cnt <= t1) {
	    p_t[i] = sysp.P_in;
	} else if(cnt < n)  {
	    p_t[i] = 0;
	} 
	cnt ++;
	if (cnt == n) {
	    cnt = 0;
	}
    }
}

void VoltageDetection::setPowerTrace() {
    printf("set power trace step = %lf s\n",sysp.pt_delta_t);
  //  if (strcmp(sysp.power_trace_type,"real") == 0) { 
    if (sysp.power_trace_type ==  1) { 
	printf("real\n");
	FILE * fp = fopen(_powerTracePath.c_str(),"r");
	if(fp != NULL) {
	    //powerTrace = new double[size];
	    double tmp; 
	    uint64_t size = 0;
	    while(fscanf(fp,"%lf\n",&tmp) != EOF) {
		size++;	
	    }
	    fclose(fp); // we should reopen it
	    p_t = new double[size];
	    fp = fopen(_powerTracePath.c_str(),"r");
	    tick_pt_end = size * 10000000;  	
	    for(int i = 0; i < size; ++i) {
		fscanf(fp,"%lf\n",&p_t[i]);
		printf("p_t[%d] = %lf uW, tick_pt_end = %ld \n",i,p_t[i],tick_pt_end);
		p_t[i] = p_t[i] * sysp.power_trace_scale/1000000;
	    }
	    fclose(fp); 	 
	} else {
	    printf("fp == NULL\n");
	    exit(1);
	}
    } else {
	generatePowerTrace();
    }
}

int VoltageDetection::getIndex(uint64_t cur_tick) {
    double cur_time = (double)cur_tick / 1000000; // get us
    return (int)(cur_time/(sysp.pt_delta_t * 1000000)); 
}

double VoltageDetection::getPowerFromGlobalState() {
    double p_out = 0;
    printf("gtype: %d\n",gtype);
    switch(gstatus) {
	case CacheReadHit:// cache reading now;
	    //apply cache read hit power
	    if(_strategy == 1) { // both caches are nonvolatile
		p_out = sysp.P_comp + p_nvm_cache_hit + sysp.P_leak + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak;
	    } else if(_strategy == 2 || _strategy == 3) {
		p_out = sysp.P_comp + p_sram_cache_hit + sysp.P_leak + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak; 
	    } else if(_strategy == 4 || _strategy == 5) { // icache is nvSRAM dcache is nvm 
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_nvm_cache_hit + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_sram_cache_hit + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_sram_cache_hit + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_nvm_cache_hit + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    }
	    break;
	case CacheReadMiss:
	    //apply cache read miss power
	    if(_strategy == 1) {
		p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak;
	    } else if(_strategy == 2 || _strategy == 3) {
		p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak; 
	    } else if(_strategy == 4 || _strategy == 5) { // icache is nvSRSAM dcache is nvm 
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    }
	    break;
	case CacheWriteHit: // should be writing the cache
	 //   p_out = sysp.P_comp + p_cache_hit + p_cache_write + sysp.P_leak + sysp.P_cache_leak + sysp.P_memory_leak; 
	    if(_strategy == 1) {
		p_out = sysp.P_comp + p_nvm_cache_hit + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak;
	    } else if(_strategy == 2 || _strategy == 3) { // both icache and dcache are nvsram
		p_out = sysp.P_comp + p_sram_cache_hit + p_sram_cache_write +  sysp.P_leak + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak; 
	    } else if(_strategy == 4 || _strategy == 5) { // icache is nvSRSAM dcache is nvm 
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_nvm_cache_hit + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  
		} else if(gtype == Inst) { 
		    p_out = sysp.P_comp + p_sram_cache_hit + p_sram_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_sram_cache_hit + p_sram_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   
		} else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_nvm_cache_hit + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  
		} 
	    }
	    break;
	case CacheWriteMiss:
	    //p_out = sysp.P_comp + p_cache_miss + sysp.P_leak + sysp.P_cache_leak + sysp.P_memory_leak;
	    if(_strategy == 1) {
		p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak;
	    } else if(_strategy == 2 || _strategy == 3) {
		p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak; 
	    } else if(_strategy == 4 || _strategy == 5) { // icache is nvSRSAM dcache is nvm 
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_sram_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_nvm_cache_miss + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    }
	    break;
	case MemRead:
	    if(_strategy == 1) {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak + p_memory_read;
	    } else if(_strategy == 2 || _strategy == 3) {
		p_out = sysp.P_comp + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak + p_memory_read;
	    } else {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak + sysp.P_leak + p_memory_read;
	    }
	    break;
	case MemWrite:
	    if(_strategy == 1) {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak + p_memory_write;
	    } else if(_strategy == 2 || _strategy == 3) { 
		p_out = sysp.P_comp + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak + p_memory_write;
	    } else {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak + sysp.P_leak + p_memory_write;
	    }
	    break;
	case Mem2CacheResp:
	    if(_strategy == 1) {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else if(_strategy == 2 || _strategy == 3) { 
		p_out = sysp.P_comp + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak + sysp.P_leak;
	    }
	    break;
	case CacheUpgrade: // must be writing the cache
	    if(_strategy == 1) {
		p_out = sysp.P_comp + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak;
	    } else if(_strategy == 2 || _strategy == 3) {
		p_out = sysp.P_comp + p_sram_cache_write + sysp.P_leak + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak; 
	    } else if(_strategy == 4 || _strategy == 5) { // icache is nvSRSAM dcache is nvm 
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_sram_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		if(gtype == Data) { // data access
		    p_out = sysp.P_comp + p_sram_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } else if(gtype == Inst) {
		    p_out = sysp.P_comp + p_nvm_cache_write + sysp.P_leak + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak;  	 	   } 
	    }
	    //p_out = sysp.P_comp + p_cache_write + sysp.P_leak + sysp.P_cache_leak + sysp.P_memory_leak; 
	    break;
	case Cache2CPUResp:
	    //p_out = sysp.P_comp + sysp.P_cache_leak + sysp.P_memory_leak + sysp.P_leak;
	    if(_strategy == 1) {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else if(_strategy == 2 || _strategy == 3) { 
		p_out = sysp.P_comp + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak + sysp.P_leak;
	    }
	    break;
	case Calculate:
	   // p_out = sysp.P_comp + sysp.P_cache_leak + sysp.P_memory_leak + sysp.P_leak;
	    if(_strategy == 1) {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else if(_strategy == 2 || _strategy == 3) { 
		p_out = sysp.P_comp + sysp.P_sram_cache_leak * 2 + sysp.P_memory_leak + sysp.P_leak;
	    } else {
		p_out = sysp.P_comp + sysp.P_nvm_cache_leak + sysp.P_sram_cache_leak + sysp.P_memory_leak + sysp.P_leak;
	    }
	    break;
    }
    return p_out;
}

double VoltageDetection::calcOutPower(int sys_state) {
    double p_out = getPowerFromGlobalState();
    switch(sys_state) {
	case ON: //  just on
	    return p_out; 
	case OFF: // just capacitor leak power
	    return sysp.P_leak;
	case ON2OFF: // still on, but soon will be off(backup)
	    return p_out;
	case OFF2ON: // still off, but soon will be on(resume)
	    return sysp.P_leak;
	case ROLLBACK2OFF:// still on
	    return p_out;
	case ROLLBACK: // still on
	    return p_out;
	case BACKUP:
	    return sysp.P_leak + p_backup; // p_backup is (global) calculated in strategy
	case RESUME:
	    return sysp.P_leak + p_resume; // p_resume is (global) calculated in strategy
    }
    return 0.0;
}

double VoltageDetection::deltaV(double V, double p_out, double p_in, double  C, double v_max, double delta_t,double& p_real_in) {
    double dP = p_in - p_out; 
    double dE = dP * delta_t;
    double delta_v = dE / (C * V);
    if( delta_v + V <= v_max)  {
	p_real_in = p_in;
	return delta_v;
    } else {
	return v_max - V;
    }
    return 0;
}

void VoltageDetection::run() {
    printf("VoltageDetection::run() called\n");
//    printf("sysp.delta_tick: %ld\n",sysp.delta_tick);
    while(1) {
	printf("run: while loop begins\n");

	#ifdef SELF_DEBUG
	printf("run: locking vd_run_mutex\n");
	#endif

	pthread_mutex_lock(&vd_run_mutex);

	#ifdef SELF_DEBUG
	printf("run: vd_run_mutex locked\n");
	#endif

	while(!vd_run_flag) {

	    printf("run: !vd_run_flag,start cond_wait\n");

	    pthread_cond_wait(&vd_run_cv,&vd_run_mutex);

	    printf("run: return from cond wait vd_run_cv\n");

	}

	vd_run_flag = false;
	pthread_mutex_unlock(&vd_run_mutex);

	//#ifdef SELF_DEBUG
	printf("run: vd_run_mutex unlocked\n");
	printf("run: locking simloop_mutex\n");
	//#endif

	pthread_mutex_lock(&simloop_mutex);

	//#ifdef SELF_DEBUG
	printf("run: simloop_mutex locked\n");
	//#endif

	printf("run: cur_tick = %ld, next_tick : %ld\n",cur_tick,tick_next);

	if(cur_tick > tick_next) {
	    printf("run: cur_tick > next_tick, set cur_tick = next_tick\n");
	    cur_tick = tick_next;

	    simloop_flag = true;

	    #ifdef SELF_DEBUG
		printf("run: set simloop_flag = true\n");
	    #endif

	    pthread_mutex_unlock(&simloop_mutex); 		 

	    pthread_cond_signal(&simloop_cv);

	    continue;
	}
	
	if(tick_next < sim_delta_tick) {
	    simloop_flag = true;

	    #ifdef SELF_DEBUG
		printf("run: set simloop_flag = true\n");
	    #endif

	    pthread_mutex_unlock(&simloop_mutex); 		 

	    pthread_cond_signal(&simloop_cv);

	    continue;
	}
	printf("run: gstatus : %d\n",gstatus);	   
	if (_strategy == 1) { // NVM cache, no back up needed, should roll back on power failure
	    // strategy == 1 : NVM icache and NVM dcache
	    strategy_1(); 
	} else if (_strategy == 2) {
	    // strategy == 2 : nvSRAM icache and nvSRAM dcache, full backup
	    strategy_2();
	} else if (_strategy == 3) { 
	    // strategy == 3 : nvSRAM icache and nvSRAM dcache, partial backup
	    strategy_2();  
	} else if (_strategy == 4) {
	    // strategy == 4 : nvSRAM icache and NVM dcache, full backup
	    strategy_2(); 
	} else if (_strategy == 5) {
	    // strategy == 5 : nvSRAM icache and NVM dcache, partial backup
	    strategy_2(); 
	} else if (_strategy == 6) {
	    // strategy == 6 : NVM icache and nvSRAM dcache, full backup
	    strategy_2(); 
	} else if (_strategy == 8) {
	    // strategy == 8 : NVM icache and nvSRAM dcache, partial backup
	    strategy_2(); 
	}

	simloop_flag = true;

	#ifdef SELF_DEBUG
	printf("run: set simloop_flag = true\n");
	#endif

	pthread_mutex_unlock(&simloop_mutex); 		 

	pthread_cond_signal(&simloop_cv);
    }
}


VoltageDetection::SysParam::SysParam() {

}


VoltageDetection *
VoltageDetectionParams::create() {
    return new VoltageDetection(this);
}


void VoltageDetection::strategy_1() {
    // nvm cache, only backup the nvff
    // no need to change
    tick_next_local = tick_next;
    double backup_t;
    double resume_t;
    nvffBackupPowerAndTime(p_backup,backup_t); // since every time is the same, we just calculate once
    nvffRestorePowerAndTime(p_resume,resume_t); // since every time is the same, we just calculate once
    backup_tick = t2tick(backup_t);
    resume_tick = t2tick(resume_t);
    // if no delay happens, tick_next_local should be equal to tick_next at the end of this function
    // else, we should add delay_tick = tick_next_local - tick_next to all the
    // pending event in the eventq
    
    while(cur_tick <= tick_pt_end) { // do until reaches the next tick
	double p_out = calcOutPower(state);
	double p_in = p_t[getIndex(cur_tick)];
	do_stat(stats,state,sim_delta_t,p_in);
	v_sys += deltaV(v_sys,p_out,p_in,sysp.C_bulk,sysp.v_max,sim_delta_t,p_real_in);
	print2file(file_vsys,file_state,file_pt,file_sys_state,v_sys,state,p_t[getIndex(cur_tick)]);
	printf("pt index: %d cur tick: %ld \n",getIndex(cur_tick),cur_tick);
	if(state == ON) {
	    if (cur_tick >= tick_next_local) {
		// we just meet the next event;
		break;	
	    }
	    printf("run: state = ON cur_tick : %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if (v_sys < sysp.v_det_backup) { // just power off now
		tick_vback = cur_tick;	
		state = ON2OFF; // give vd t_vd_phl time to react 
	    }
	} else if(state == ON2OFF) {
	    printf("run: state = ON2OFF cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vback + phl_tick) {
		state = BACKUP;	 // backup signal generated, start backup NVFF
		tick_backup_start = cur_tick;
		rollback_tick = tick_backup_start - tick_rollback_start; // calculate the rollback tick
	    }
	} else if(state == BACKUP) {
	    printf("run: state = BACKUP, backup tick = %ld cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n", backup_tick,cur_tick,tick_pt_end,p_in,v_sys);
	    if (cur_tick >= tick_backup_start + backup_tick) {
		tick_off = cur_tick;
		state = OFF; // go to off
		delay_tick_local += backup_tick; //add the backup tick to the delay  	
	    }
	      	 	    
	} else if(state == OFF) {
	    printf("run: state = OFF cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if (v_sys > sysp.v_det_restore) {
		tick_vrestore = cur_tick;
		state = OFF2ON;
	    }
	} else if(state == OFF2ON) {
	    printf("run: state = OFF2ON cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vrestore + plh_tick) {
		state = RESUME; // should start the resume;
		tick_resume_start = cur_tick; // start to restore CPU state from NVFF
		delay_tick_local += tick_resume_start - tick_off;// add the off time to the delay 	  	
	    }
	} else if(state == RESUME) {
	    printf("run: state = RESUME, resume tick = %ld cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",resume_tick,cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_resume_start + resume_tick) {
		tick_rollback_start_local = cur_tick; // start to roll back
		state = ROLLBACK; // start to roll back
		delay_tick_local += resume_tick; // add the resume time to delay
	    }

	} else if(state == ROLLBACK) {// roll back is still a kind of running state
	    printf("run: state = ROLLBACK cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if (v_sys < sysp.v_det_backup) { // when during rolling back, if power fails again, roll back again
		tick_vback = cur_tick;
		state = ROLLBACK2OFF;
		delay_tick_local += cur_tick + phl_tick - tick_rollback_start_local;// add the part roll back time to delay 
	    } else {
		if(cur_tick > tick_rollback_start_local + rollback_tick) { // finish the roll back stage, should go on, next_tick should be added a delay
		    state = ON;	 	 // roll back finished
		    delay_tick_local += rollback_tick; // add the rollback tick to the delay
		    tick_next_local +=  delay_tick_local;
		    delay_tick_local = 0; // reset the local delay tick to finish the rest of the time till the tick_next_local, if new delays happens, it will record them;
		}
	    }
	} else if(state == ROLLBACK2OFF) { // roll back not finished, off again
	    printf("run: state = ROLLBACK2OFF cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vback + phl_tick) {
		state = BACKUP; // the off signal generated
		tick_backup_start = cur_tick;
	    }
	}
	cur_tick += sim_delta_tick;
    }
    stats.cap_diff_energy = getCapEnergyDiff(v_init,v_sys);

    printStats();

    if(cur_tick >= tick_pt_end) {
	printf("running out of power trace, but still not end!!"); 
	exit(0);
    }
    assert(tick_next_local >= tick_next);
    if(tick_next_local > tick_next) { // means that rollbal/off has happened, we should reschedule rest of the events in the eventq
	resched_flag = true;	
	delay_tick  = tick_next_local - tick_next;
	DPRINTF(VoltageDetection,"%s delayed tick: %ld\n",__func__,delay_tick);
    }
}

double VoltageDetection::getCapEnergyDiff(double init, double cur) {
    return 0.5 * sysp.C_bulk * cur * cur - 0.5 * sysp.C_bulk * init * init;
}

void VoltageDetection::strategy_2() {
    // sram cache, backup nvff and cache content, also should rollback
    // only backup the valid cache content, the invalid ones should never be  backed up
    // nvff and cache backup in parallel
    // slight modification, consider icache and dcache seperately
    uint64_t min_backup_tick = 0;
    uint64_t min_resume_tick = 0; 
    uint64_t max_backup_tick = 0;
    uint64_t max_resume_tick = 0; 

    tick_next_local = tick_next;
    double backup_nvff_t;
    double resume_nvff_t;
    nvffBackupPowerAndTime(p_nvff_backup,backup_nvff_t); // since every time is the same, we just calculate once
    nvffRestorePowerAndTime(p_nvff_resume,resume_nvff_t); // since every time is the same, we just calculate once
    backup_nvff_tick = t2tick(backup_nvff_t);
    resume_nvff_tick = t2tick(resume_nvff_t);

    //just read all the nvSRAM nonvolatile backup cell into the sram, should be const through out the operation
    double resume_nvsram_t; 
    nvSRAMRestorePowerAndTime(p_nvsram_resume, resume_nvsram_t); // now we get the nvSRAM restore bits accordingly
    resume_nvsram_tick = t2tick(resume_nvsram_t);

    if(resume_nvsram_tick < resume_nvff_tick) { // we do the resume in parallel, we want to know who finishes resume first
	min_resume_tick = resume_nvsram_tick;
	max_resume_tick = resume_nvff_tick;
    } else {
	min_resume_tick = resume_nvff_tick;
	max_resume_tick = resume_nvsram_tick;
    }

    // if no delay happens, tick_next_local should be equal to tick_next at the end of this function
    // else, we should add delay_tick = tick_next_local - tick_next to all the
    // pending event in the eventq
    
    while(cur_tick <= tick_pt_end) { // do until reaches the next tick
	double p_out = calcOutPower(state);
	double p_in = p_t[getIndex(cur_tick)];
	do_stat(stats,state,sim_delta_t,p_in);
	v_sys += deltaV(v_sys,p_out,p_in,sysp.C_bulk,sysp.v_max,sim_delta_t,p_real_in);
	print2file(file_vsys,file_state,file_pt,file_sys_state,v_sys,state,p_t[getIndex(cur_tick)]);

	if(state == ON) {
	    if (cur_tick >= tick_next_local) {
		break;	
	    }
	    printf("run: state = ON cur_tick : %ld tick_pt_end: %ld p_in: %lf v_sys: %lf tick_next_local: %ld\n",cur_tick,tick_pt_end,p_in,v_sys,tick_next_local);
	    if (v_sys < sysp.v_det_backup) { // just power off now
		tick_vback = cur_tick;	
		state = ON2OFF; // give vd t_vd_phl time to react 
	    }
	} else if(state == ON2OFF) {
	    printf("run: state = ON2OFF cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vback + phl_tick) {
		state = BACKUP;	 // backup signal generated, start backup NVFF
		tick_backup_start = cur_tick;
		rollback_tick = tick_backup_start - tick_rollback_start; // calculate the rollback tick
		// backup the content in the cache, should only backup the valid cache blocks
		double backup_nvsram_t; 
		if (_strategy == 2 || _strategy == 4 || _strategy == 6) { // the full backup
		    nvSRAMBackupPowerAndTime("all", p_nvsram_backup, backup_nvsram_t); 
		}else if(_strategy == 3 || _strategy == 5 || _strategy == 8) {
		    nvSRAMBackupPowerAndTime("lru", p_nvsram_backup, backup_nvsram_t);// the lru partial backup
		}
		// now we get the nvSRAM backup bits accordingly	
		backup_nvsram_tick = t2tick(backup_nvsram_t);
		if(backup_nvsram_tick < backup_nvff_tick) { // because we will do in parallel , we want to know who finished first
		    min_backup_tick = backup_nvsram_tick;
		    max_backup_tick = backup_nvff_tick;
		} else {
		    min_backup_tick = backup_nvff_tick;
		    max_backup_tick = backup_nvsram_tick;
		}
		p_backup = p_nvsram_backup + p_nvff_backup; // by the start, the backup power must be the add of two 
	    }
	} else if(state == BACKUP) {
	    printf("run: state = BACKUP, backup tick = %ld cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n", backup_tick,cur_tick,tick_pt_end,p_in,v_sys);
	    if (cur_tick > tick_backup_start + min_backup_tick && cur_tick < tick_backup_start + backup_nvff_tick) {
		// nvsram backup finished but nvff backup not finished, should  be a rare case
		p_backup = p_nvff_backup;	
	    } else if (cur_tick > tick_backup_start + min_backup_tick && cur_tick < tick_backup_start + backup_nvsram_tick) {
		// nvff backup finished but nvsram backup not finished  
		p_backup = p_nvsram_backup;
	    } else if (cur_tick >= tick_backup_start + max_backup_tick) { // means that we finish the backup, should be off from now on
		tick_off = cur_tick;
		state = OFF;
		delay_tick_local += max_backup_tick; // the total backup time should be the max of the two
	    }

	} else if(state == OFF) {
	    printf("run: state = OFF cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if (v_sys > sysp.v_det_restore) {
		tick_vrestore = cur_tick;
		state = OFF2ON;
	    }
	} else if(state == OFF2ON) {
	    printf("run: state = OFF2ON cur_tick: %ld tick_pt_end: %ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vrestore + plh_tick) {
		state = RESUME; // should start the resume;
//		state = ROLLBACK;//rolling back to the start of the operation
//		tick_on = cur_tick;// record the tick when on again
		tick_resume_start = cur_tick; // start to restore CPU state from NVFF
//		tick_rollback_start_local = tick_on;
		//since the interrupted operation restart from now, we should update the last state tick to current tick;
//		delay_tick_local += tick_on - tick_off;// add the off time to the delay 	  	
		delay_tick_local += tick_resume_start - tick_off;// add the off time to the delay 	  	
		p_resume = p_nvsram_resume + p_nvff_resume; // these two are pre-calculated
	    }
	} else if(state == RESUME) {
	    printf("run: state = RESUME, resume tick = %ld cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",resume_tick,cur_tick,tick_pt_end,p_in,v_sys);
	    if (cur_tick > tick_resume_start + min_resume_tick && cur_tick < tick_resume_start + resume_nvff_tick) {
		// nvsram resume finished but nvff resume not finished, should  be a rare case
		p_resume = p_nvff_resume;	
	    } else if (cur_tick > tick_resume_start + min_resume_tick && cur_tick < tick_resume_start + resume_nvsram_tick) {
		// nvff resume finished but nvsram resume not finished  
		p_resume = p_nvsram_resume;
	    } else if (cur_tick >= tick_resume_start + max_resume_tick) { // means that we finish the resume,  should be on from now on
		tick_off = cur_tick;
		state = ROLLBACK;
		delay_tick_local += max_resume_tick; // the total backup time should be the max of the 
	    }

	} else if(state == ROLLBACK) {// roll back is still a kind of running state
	    printf("run: state = ROLLBACK cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if (v_sys < sysp.v_det_backup) { // when during rolling back, if power fails again, roll back again
		tick_vback = cur_tick;
		state = ROLLBACK2OFF;
		delay_tick_local += cur_tick + phl_tick - tick_rollback_start_local;// add the part roll back time to delay 
	    } else {
		if(cur_tick > tick_rollback_start_local + rollback_tick) { // finish the roll back stage, should go on, next_tick should be added a delay
		    state = ON;	 	 // roll back finished
		    delay_tick_local += rollback_tick; // add the rollback tick to the delay
		    tick_next_local +=  delay_tick_local;
		    delay_tick_local = 0; // reset the local delay tick to finish the rest of the time till the tick_next_local, if new delays happens, it will record them;
		}
	    }
	} else if(state == ROLLBACK2OFF) { // roll back not finished, off again
	    printf("run: state = ROLLBACK2OFF cur_tick: %ld tick_pt_end:%ld p_in: %lf v_sys: %lf\n",cur_tick,tick_pt_end,p_in,v_sys);
	    if(cur_tick >= tick_vback + phl_tick) {
		state = BACKUP; // the off signal generated
		// when roll back is interrupted, one thing to be mentioned is
		// that we are acutally doing some stuff, but not finished
		// until the gstatus changes, for example, if we are reading
		// memory and got power failure, when we roll back, we are
		// still doing the memory reading, so the cache content and
		// cpu content will not change, so the result of backup power
		// calculation should be the same; we can skip the calculation if we want
		double backup_nvsram_t; // we have to backup again; 
		if (_strategy == 2 || _strategy == 4 || _strategy == 6) {
		    nvSRAMBackupPowerAndTime("all", p_nvsram_backup, backup_nvsram_t); // the full backup
		}else if(_strategy == 3 || _strategy == 5 || _strategy == 8) {
		    nvSRAMBackupPowerAndTime("lru", p_nvsram_backup, backup_nvsram_t); // the lru partial backup
		}
		// now we get the nvSRAM backup bits accordingly;
		backup_nvsram_tick = t2tick(backup_nvsram_t);
		if(backup_nvsram_tick < backup_nvff_tick) { // because we will do in parallel , we want to know who finished first
		    min_backup_tick = backup_nvsram_tick;
		    max_backup_tick = backup_nvff_tick;
		} else {
		    min_backup_tick = backup_nvff_tick;
		    max_backup_tick = backup_nvsram_tick;
		}
		p_backup = p_nvsram_backup + p_nvff_backup; // by the start, the backup power must be the add of two 
		tick_backup_start = cur_tick;
	    }
	}
	cur_tick += sim_delta_tick;
    }
    stats.cap_diff_energy = getCapEnergyDiff(v_init,v_sys);
    printStats();
    if(cur_tick >= tick_pt_end) {
	printf("running out of power trace, but still not end!!"); 
	exit(0);
    }
    assert(tick_next_local >= tick_next);
    if(tick_next_local > tick_next) { // means that rollbal/off has happened, we should reschedule rest of the events in the eventq
	resched_flag = true;	
	delay_tick  = tick_next_local - tick_next;
	DPRINTF(VoltageDetection,"%s delayed tick: %ld\n",__func__,delay_tick);
    }
}

void VoltageDetection::strategy_3() {

}

void VoltageDetection::nvffBackupPowerAndTime(double & p_backup, double& t_backup) {
    int N = 1607; // magic number of toal NVFF to be backed up // based on THU1010N configuration
    double P_write = N * p_nvff_store;	
    if (P_write <= sysp.P_max) {
	p_backup = P_write;
	t_backup = sysp.t_nvff_store;
    } else {
	p_backup = sysp.P_max;
	t_backup = sysp.t_nvff_store * (sysp.P_max/P_write);
    }
}

void VoltageDetection::nvffRestorePowerAndTime(double & p_resume, double& t_resume) {
    int N = 1607; // magic number of toal NVFF to be backed up
    double P_read = N * p_nvff_restore;	
    if (P_read <= sysp.P_max) {
	p_resume = P_read;
	t_resume = sysp.t_nvff_restore;
    } else {
	p_resume = sysp.P_max;
	t_resume = sysp.t_nvff_restore * (sysp.P_max/P_read);
    }
}

void VoltageDetection::nvSRAMBackupPowerAndTime(const char* strategy, double & p_backup, double& t_backup) {
    uint64_t N = 0;
    if(strcmp(strategy,"all") == 0) { // all backup, get the valid cache line number, need the pointer to the cache 
	uint64_t dcache_backup_bit = _dcache->cacheBackup(0);
	uint64_t icache_backup_bit = _icache->cacheBackup(0);
	printf("dcache backup bit = %ld icache backup bit = %ld \n", dcache_backup_bit, icache_backup_bit);	
	if(_strategy == 2) {
	    N = dcache_backup_bit + icache_backup_bit;  
	} else if(_strategy == 4) {
	    N = icache_backup_bit;
	} else if(_strategy == 6) {
	    N = dcache_backup_bit;
	}
    } else if(strcmp(strategy,"lru") == 0) { // part backup the cache, need invalidate the valid but discarded cache line
	if(_strategy == 3) { // 
	    // strategy == 3 : nvSRAM icache and nvSRAM dcache, partial backup
	    uint64_t dcache_backup_bit = _dcache->cacheBackup(_threshold);
	    uint64_t icache_backup_bit = _icache->cacheBackup(_threshold); // when get the number of bits to backup, some blocks are invalidted too
	    N = dcache_backup_bit + icache_backup_bit;
	} else if(_strategy == 5) {
	    // strategy == 5 : nvSRAM icache and NVM dcache, partial backup
	    N = _icache->cacheBackup(_threshold);   
	} else if(_strategy == 8) {
	    // strategy == 8 : NVM icache and nvSRAM dcache, partial backup
	    N = _dcache->cacheBackup(_threshold);
	}
    }
    double P_write = N * p_nvsram_store;	
    if (P_write <= sysp.P_max) {
	p_backup = P_write;
	t_backup = sysp.t_nvsram_store; // all in parallel
    } else {
	p_backup = sysp.P_max;
	t_backup = sysp.t_nvsram_store * (sysp.P_max/P_write);
    }
}


void VoltageDetection::nvSRAMRestorePowerAndTime(double & p_resume, double& t_resume) { 
    uint64_t dcache_resume_bit = _dcache->cacheInfo(); 
    uint64_t icache_resume_bit = _dcache->cacheInfo();
    printf("dcache resume bit = %ld icache resume bit = %ld \n", dcache_resume_bit, icache_resume_bit);	
    uint64_t N = 0; 

    // get the nvSRAM bits to be resumed according to the strategies; 
    if(_strategy == 2 || _strategy == 3) {
	N = dcache_resume_bit + icache_resume_bit;  
    } else if(_strategy == 4 || _strategy == 5) {
	N = icache_resume_bit;
    } else if(_strategy == 6 || _strategy == 8) {
	N = dcache_resume_bit;
    }

    double P_read = N * p_nvsram_restore;	
    if (P_read <= sysp.P_max) {
	p_resume = P_read;
	t_resume = sysp.t_nvsram_restore;
    } else {
	p_resume = sysp.P_max;
	t_resume = sysp.t_nvsram_restore * (sysp.P_max/P_read);
    }
}


void VoltageDetection::do_stat(struct Statistics& stat,int sys_state, double delta_t,double p_in) {
    stat.input_energy += p_in * delta_t;
    if(sys_state == ON || sys_state == ON2OFF || sys_state == ROLLBACK || sys_state == ROLLBACK2OFF) {  
	// in these system states, no clock gating activated
	switch(gstatus) {
	    case CacheReadHit:// cache reading now;
	    //apply cache read hit power
		if(_strategy == 1) { 
		    stat.cache_readhit_energy += p_nvm_cache_hit * delta_t; // read hit for nvm cache
		} else if(_strategy == 4 || _strategy == 5) { // dcache is nvm, icache is sram
		    if(gtype == Data) { // accessing the nvm cache
			stat.cache_readhit_energy += p_nvm_cache_hit * delta_t;
		    } else if(gtype == Inst) { // accessing the sram cache
			stat.cache_readhit_energy += p_sram_cache_hit * delta_t;
		    }
		} else if(_strategy == 2 || _strategy == 3) { // read hit for sram cache
		    stat.cache_readhit_energy += p_sram_cache_hit * delta_t;
		} else if(_strategy == 6 || _strategy == 8) { // dcache is sram, icache is rram
		    if(gtype == Data) {	
			stat.cache_readhit_energy += p_sram_cache_hit * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_readhit_energy += p_nvm_cache_hit * delta_t;
		    }
		}
		stat.other_energy += sysp.P_comp * delta_t;
		stat.cache_readhit_time += delta_t;
		break;
	    case CacheReadMiss:
	    //apply cache read miss powre
		if(_strategy == 1) { 
		    stat.cache_readmiss_energy += p_nvm_cache_miss * delta_t; // read hit for nvm cache
		} else if(_strategy == 4 || _strategy == 5) { // dcache is nvm, icache is sram
		    if(gtype == Data) { // accessing the nvm cache
			stat.cache_readmiss_energy += p_nvm_cache_miss * delta_t;
		    } else if(gtype == Inst) { // accessing the sram cache
			stat.cache_readmiss_energy += p_sram_cache_miss * delta_t;
		    }
		} else if(_strategy == 2 || _strategy == 3) { // read hit for sram cache
		    stat.cache_readmiss_energy += p_sram_cache_miss * delta_t;
		} else if(_strategy == 6 || _strategy == 8) { // dcache is sram, icache is rram
		    if(gtype == Data) {	
			stat.cache_readmiss_energy += p_sram_cache_miss * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_readmiss_energy += p_nvm_cache_miss * delta_t;
		    }
		}
		stat.other_energy += sysp.P_comp * delta_t;
		stat.cache_readmiss_time += delta_t;
		break;
	    case CacheWriteHit: // should be writing the cache
		if(_strategy == 1) { 
		    stat.cache_writehit_energy += p_nvm_cache_hit * delta_t; // read hit for nvm cache
		    stat.cache_write_energy += p_nvm_cache_write * delta_t;
		} else if(_strategy == 4 || _strategy == 5) { // dcache is nvm, icache is sram
		    if(gtype == Data) { // accessing the nvm cache
			stat.cache_writehit_energy += p_nvm_cache_hit * delta_t;
			stat.cache_write_energy += p_nvm_cache_write * delta_t;
		    } else if(gtype == Inst) { // accessing the sram cache
			stat.cache_writehit_energy += p_sram_cache_hit * delta_t;
			stat.cache_write_energy += p_sram_cache_write * delta_t;
		    }
		} else if(_strategy == 2 || _strategy == 3) { // read hit for sram cache
		    stat.cache_writehit_energy += p_sram_cache_hit * delta_t;
		    stat.cache_write_energy += p_sram_cache_write * delta_t;
		} else if(_strategy == 6 || _strategy == 8) { // dcache is sram, icache is rram
		    if(gtype == Data) {	
			stat.cache_writehit_energy += p_sram_cache_hit * delta_t;
			stat.cache_write_energy += p_sram_cache_write * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_writehit_energy += p_nvm_cache_hit * delta_t;
			stat.cache_write_energy += p_nvm_cache_write * delta_t;
		    }
		}
		stat.other_energy += sysp.P_comp * delta_t;
		stat.cache_writehit_time += delta_t;
		break;
	    case CacheWriteMiss:
		if(_strategy == 1) { 
		    stat.cache_writemiss_energy += p_nvm_cache_miss * delta_t; // read hit for nvm cache
		} else if(_strategy == 4 || _strategy == 5) { // dcache is nvm, icache is sram
		    if(gtype == Data) { // accessing the nvm cache
			stat.cache_writemiss_energy += p_nvm_cache_miss * delta_t;
		    } else if(gtype == Inst) { // accessing the sram cache
			stat.cache_writemiss_energy += p_sram_cache_miss * delta_t;
		    }
		} else if(_strategy == 2 || _strategy == 3) { // read hit for sram cache
		    stat.cache_writemiss_energy += p_sram_cache_miss * delta_t;
		} else if(_strategy == 6 || _strategy == 8) { // dcache is sram, icache is rram
		    if(gtype == Data) {	
			stat.cache_writemiss_energy += p_sram_cache_miss * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_writemiss_energy += p_nvm_cache_miss * delta_t;
		    }
		}
		stat.other_energy += sysp.P_comp * delta_t;
		stat.cache_writemiss_time += delta_t;
		break;
	    case MemRead:
		stat.other_energy += sysp.P_comp * delta_t;
		stat.mem_read_energy += p_memory_read * delta_t;
		stat.mem_read_time += delta_t;
		break;
	    case MemWrite:
		stat.other_energy += sysp.P_comp * delta_t;
		stat.mem_write_energy += p_memory_write * delta_t;
		stat.mem_write_time += delta_t;
		break;
	    case Mem2CacheResp:
		stat.other_energy += sysp.P_comp * delta_t;
		stat.other_time += delta_t;
		break;
	    case CacheUpgrade: // must be writing the cache
		if(_strategy == 1) {
		    stat.cache_write_energy += p_nvm_cache_write * delta_t;
		} else if(_strategy == 2 || _strategy == 3) {
		    stat.cache_write_energy += p_sram_cache_write * delta_t;
		} else if(_strategy == 4 || _strategy == 5) { // icache is nvSRSAM dcache is nvm 
		    if(gtype == Data) { // data access
			stat.cache_write_energy += p_nvm_cache_write * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_write_energy += p_sram_cache_write * delta_t;
		    }
	        } else if(_strategy == 6 || _strategy == 8) { // icache is nvm and dcache is nvsram
		    if(gtype == Data) { // data access
			stat.cache_write_energy += p_sram_cache_write * delta_t;
		    } else if(gtype == Inst) {
			stat.cache_write_energy += p_nvm_cache_write * delta_t;
		    } 
		}
		stat.other_energy += sysp.P_comp * delta_t;
		stat.cache_write_time += delta_t;
		break;
	    case Cache2CPUResp:
		stat.other_energy += sysp.P_comp * delta_t;
		stat.other_time += delta_t;
		break;
	    case Calculate:
		stat.other_energy += sysp.P_comp * delta_t;
		stat.other_time += delta_t;
		break;
    	}
	// we have these leakage 
	stat.cap_leak_energy += sysp.P_leak * delta_t; 
	if(_strategy == 1) {
	    stat.cache_leak_energy += sysp.P_nvm_cache_leak * 2 * delta_t;
	} else if(_strategy == 2 || _strategy == 3) {
	    stat.cache_leak_energy += sysp.P_sram_cache_leak * 2 * delta_t;
	} else {
	    stat.cache_leak_energy += (sysp.P_sram_cache_leak + sysp.P_nvm_cache_leak) * delta_t;
	}
	stat.mem_leak_energy += sysp.P_memory_leak * delta_t;
    } else if (sys_state == BACKUP) {
	stat.backup_energy += p_backup * delta_t;
	stat.cap_leak_energy += sysp.P_leak * delta_t; 
	stat.backup_time += delta_t;

    } else if (sys_state == RESUME) {
	stat.resume_energy += p_resume * delta_t;
	stat.resume_time += delta_t;		
	stat.cap_leak_energy += sysp.P_leak * delta_t; 
    } else if (sys_state == OFF || sys_state == OFF2ON) {
	stat.cap_leak_energy += sysp.P_leak * delta_t; 
	stat.off_time += delta_t;
    }
}

void VoltageDetection::Statistics::print_stats(FILE * fp) {
	    double billion = 1000000000;
	    double trillion = billion * 1000;
	    double total_energy = backup_energy + resume_energy + cache_readhit_energy + cache_readmiss_energy + cache_writehit_energy + cache_writemiss_energy + cache_write_energy + mem_read_energy + mem_write_energy + other_energy + cache_leak_energy + cap_leak_energy + mem_leak_energy;

	    fprintf(fp, "total energy: %lf nJ\n", total_energy * trillion);
	    fprintf(fp, "  -- backup_energy: %lf nJ \n", backup_energy*trillion);        
	    fprintf(fp, "  -- resume_energy: %lf nJ \n",resume_energy*trillion);
	    fprintf(fp, "  -- cache_readhit_energy: %lf nJ \n",cache_readhit_energy*trillion);
	    fprintf(fp, "  -- cache_readmiss_energy: %lf nJ \n",cache_readmiss_energy*trillion);
	    fprintf(fp, "  -- cache_writehit_energy: %lf nJ \n",cache_writehit_energy*trillion);
	    fprintf(fp, "  -- cache_writemiss_energy: %lf nJ \n",cache_writemiss_energy*trillion);
	    fprintf(fp, "  -- cache_write_energy: %lf nJ \n",cache_write_energy*trillion);
	    fprintf(fp, "  -- mem_read_energy: %lf nJ \n",mem_read_energy*trillion);
	    fprintf(fp, "  -- mem_write_energy: %lf nJ \n",mem_write_energy*trillion);
	    fprintf(fp, "  -- other_energy: %lf nJ \n",other_energy*trillion);
	    fprintf(fp, "  -- cache_leak_energy: %lf nJ \n",cache_leak_energy*trillion);
	    fprintf(fp, "  -- cap_leak_energy: %lf nJ \n",cap_leak_energy*trillion);
	    fprintf(fp, "  -- mem_leak_energy: %lf nJ \n",mem_leak_energy*trillion);
		
	    fprintf(fp, "input_energy: %lf nJ \n",input_energy*trillion);
	    fprintf(fp, "cap diff energy: %lf nJ \n",cap_diff_energy*trillion);

	    double total_time = backup_time + resume_time + cache_readhit_time + cache_writehit_time + cache_writemiss_time + mem_read_time + mem_write_time + other_time + off_time  + cache_readmiss_time;

	    fprintf(fp, "***********************\n");
	    fprintf(fp, "total time: %lf ns\n", total_time * billion);
	    fprintf(fp, "backup_time: %lf ns \n",backup_time*billion);
	    fprintf(fp, "resume_time: %lf ns \n",resume_time*billion);
	    fprintf(fp, "cache_readhit_time: %lf ns \n",cache_readhit_time*billion);
	    fprintf(fp, "cache_readmiss_time: %lf ns \n",cache_readmiss_time*billion);
	    fprintf(fp, "cache_writehit_time: %lf ns \n",cache_writehit_time*billion);
	    fprintf(fp, "cache_writemiss_time: %lf ns \n",cache_writemiss_time*billion);
	    fprintf(fp, "mem_read_time: %lf ns \n",mem_read_time*billion);
	    fprintf(fp, "mem_write_time: %lf ns \n",mem_write_time*billion);
	    fprintf(fp, "other_time: %lf ns \n",other_time*billion);
	    fprintf(fp, "off_time: %lf ns \n",off_time*billion);

}

void VoltageDetection::printStats() {
    file_et_stats = fopen("energy_time_stats.txt","w");
    stats.print_stats(file_et_stats);
    fclose(file_et_stats);
}
