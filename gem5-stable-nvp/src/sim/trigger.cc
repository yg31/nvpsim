#include "debug/Trigger.hh"
#include "sim/eventq_impl.hh"
#include "params/Trigger.hh"
#include "sim/eventq.hh"
#include "sim/trigger.hh"

//Trigger::Trigger(const Params *p) : SimObject(p),backupEvent(p->cpu,p->cache),resumeEvent(p->cpu,p->cache),backupStart(p->backup_start),resumeStart(p->resume_start),pro_file(p->pro_file) {
Trigger::Trigger(const Params *p) : SimObject(p),backupEvent(p->cpu),resumeEvent(p->cpu),backupStart(p->backup_start),resumeStart(p->resume_start),pro_file(p->pro_file) {
    int t_backup, t_resume;   
    DPRINTF(Trigger,"%s\n",__func__);
    FILE * fp = fopen(pro_file.c_str(),"r");   
   // FILE * fp = fopen("/home/gyz/code/architecture/fullsys/gem5-stable-nvp/build/ARM/profile","r");
    if(fp != NULL) {
	while(fscanf(fp,"%d %d\n",&t_backup,&t_resume)) {
	    if(t_backup == -1)
		break;
	    BackupEvent *bE = new BackupEvent(p->cpu);
	    ResumeEvent *rE = new ResumeEvent(p->cpu);
	    if(t_backup >= 0 && t_resume >= t_backup) {
		schedule(*bE,Cycles(t_backup));  
		schedule(*rE,Cycles(t_resume));  
	    }
	}
    }// else {
//	panic("cannot open file\n");
 //   }
    /*  
    if(pro_file != "") {
	DPRINTF(Trigger,"%s\n",pro_file.c_str());
	FILE * fp = fopen(pro_file.c_str(),"r");
	if(fp != NULL) {
	    while(fscanf(fp,"%d %d\n",&t_backup,&t_resume)) {
		if(t_backup == -1)
		    break;
		if(t_backup >= 0 && t_resume >= t_backup) {
		    schedule(backupEvent,Cycles(t_backup));  
		    schedule(resumeEvent,Cycles(t_resume));  
		}
	    }
	} else {
	    panic("cannot open file\n");
	}
    } else if(backupStart <= resumeStart && backupStart != 0) {
	DPRINTF(Trigger,"%s, not file input\n",__func__);
	schedule(backupEvent,Cycles(backupStart));  
	schedule(resumeEvent,Cycles(resumeStart));  
    }
    */
}

/*  
Trigger::BackupEvent::BackupEvent(BaseSimpleCPU * cpu,BaseCache * cache) : Event(Backup_Pri),baseCpu(cpu),baseCache(cache){

}


Trigger::ResumeEvent::ResumeEvent(BaseSimpleCPU * cpu, BaseCache * cache) : Event(Resume_Pri,s,0),baseCpu(cpu),baseCache(cache) {

}

*/

Trigger::BackupEvent::BackupEvent(BaseSimpleCPU * cpu) : Event(Backup_Pri),baseCpu(cpu) {

}


Trigger::ResumeEvent::ResumeEvent(BaseSimpleCPU * cpu) : Event(Resume_Pri),baseCpu(cpu) {

}

void 
Trigger::BackupEvent::process() {
    DPRINTF(Trigger,"BackupEvent\n");
    if(baseCpu != NULL) 
	baseCpu->getSimpleThread()->suspend();
    else
	DPRINTF(Trigger,"baseCpu == NULL\n");

  //  baseCache->cpuSidePort->flushQueue();
    DPRINTF(Trigger,"after flush pkt q, test \n");
}

void 
Trigger::ResumeEvent::process() {
    DPRINTF(Trigger,"ResumeEvent\n");
    //simpleThread->activate(Cycles(1));
   // baseCpu->getSimpleThread()->activate(Cycles(2));
    baseCpu->getSimpleThread()->wakeup();
    
}

void Trigger::regStats() {

}

Trigger *
TriggerParams::create() {
    return new Trigger(this);
}

const char *
Trigger::BackupEvent::description() const {
    return "BackupEvent";
}

const char *
Trigger::ResumeEvent::description() const {
    return "ResumeEvent";
}
