from m5.SimObject import SimObject
from m5.params import *

class VoltageDetection(SimObject):
    type = 'VoltageDetection'
    cxx_header = "sim/voltage_detection.hh"
    cpu = Param.BaseSimpleCPU("BaseSimpleCPU")
    vd_on = Param.Int(0,"vd_on");
    strategy = Param.Int(0,"strategy");  
    threshold = Param.Int(0,"lru threshold"); 
    dcache = Param.BaseCache("DBaseCache")
    icache = Param.BaseCache("IBaseCache")
    sysParamPath = Param.String("","sysParamPath")
    powerTracePath = Param.String("","powerTracePath")
