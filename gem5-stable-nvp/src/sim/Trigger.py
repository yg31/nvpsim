from m5.SimObject import SimObject 
from m5.params import *

class Trigger(SimObject):
    type = 'Trigger'
    cxx_header = "sim/trigger.hh"
    cpu = Param.BaseSimpleCPU("BaseSimpleCPU")
    #cache = Param.BaseCache("BaseCache") 
    backup_start = Param.UInt64(0,"Backup Start")
    resume_start = Param.UInt64(0,"Resume Start")
    pro_file = Param.String("","backup/resume time stamp")
