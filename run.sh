#!/bin/bash
# Example for running the NVPsim

#Modify the following paths to reflect your own setting

Script=/your/dir/to/gem5-stable-nvp/configs/mibench/runmibench.py
Gem5=/your/dir/to/gem5-stable-nvp/build/ARM/gem5.opt
PythonPath=/your/dir/to/gem5-stable-nvp/build/ARM
SysParamDir=/your/dir/to/SysParam/system_param
PowerTraceRoot=/your/dir/to/power

bench=FFT

th=200
dc=0.5

PYTHONPATH=$PythonPath $Gem5 --outdir="/tmp/out" \
		--stats-file=stats.txt \
		$Script --cpu-type=timing --caches --l1d_assoc=2 --l1i_assoc=2 \
		--l1d_size=8kB --l1i_size=8kB --cpu-clock=1500MHz --maxinsts=5000 --mem-type=lpddr3_1600_x32 --mem-size=16MB  \
		--sys_param_file=$SysParamDir \
		--power_trace_file="$PowerTraceRoot/RFHome_10us_30s_uW.txt" \
		--vd_on=1 \
		--strategy=1 \
		--threshold=$th \
		--benchmark="$bench"
